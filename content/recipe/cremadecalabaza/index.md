---
title: "Crema de calabaza"
date: 2020-11-23T16:25:15+01:00
draft: false
description: "Esta simple crema de calabaza es uno de los platos más sencillos de hacer y más sabrosos. Haremos un plato rico, vegano y sin gluten." #"Description of the post (displayed in the post's card)"
categories: [] #["Add comma separated categories here", "another category"]
toc: false #if the post should include a table of contents (true, false)
displayInMenu: false #if the post should show up in the navigation menu (true, false)
displayInList: true #if the post should be listed on the home page and category pages (true, false)
resources:
- name: featuredImage #featuredImage
  src: "calabaza.jpg" #"Filename of the post's featured image, used as the card image and the image at the top of the article"
  params:
    description: "Imagen del plato" #"Description for the featured image, used as the alt text"
---

Esta simple crema de calabaza es uno de los platos más sencillos de hacer y más sabrosos. Haremos un plato rico, vegano y sin gluten.

Para este plato necesitaremos los siguientes ingredientes: 
- 75ml AOVE
- Media Calabaza (Preferiblemente sin cortar y empaquetar)
- 300g de Zanahorias

## Preparación 

1. Pelaremos las zanahorias y las cortaremos en taquitos de unos 2 cm de lado
1. Pelaremos la calabaza y la cortaremos en cubos de unos 4cm de lado 
1. En un cazo echaremos un poquito de aceite para evitar que se peguen y lo pondremos con el fuego a tope
1. Cuando el cazo esté suficientemente caliente, echaremos la zanahoria y calabaza.
1. Moveremos la verdura continuamente para evitar que se cocine por completo, queremos sellarla solamente 
1. Cuando la verdura oscurezca y dore levemente, la cubriremos con agua y bajaremos el fuego al mínimo
1. Dejaremos hervir tapando el cazo durante unos 40 min
1. Pincharemos con un cuchillo algún trozo de zanahoria, cuando esta no ofrezca resistencia, la coción estará terminada
1. Colaremos la verdura y guardaremos el caldo en un recipiente separado
1. Trituramos las verduras con la batidora y añadimos el AOVE
1. Integraremos el caldo de la cocción que hemos colado anteriormente sin dejar de batir hasta lograr la textura cremosa deseada
1. Salpimentamos al gusto

## Presentación 

Colocando la crema en un plato hondo, con un par de hojitas de perejil lo tendreis hecho. Agregad un hilo de aceite de trufa para conseguir "El toque"

## Diagrama de la receta.
![Imagen del diagrama de la receta](diagram.png)