---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
draft: true
description: "" #"Description of the post (displayed in the post's card)"
categories: [] #["Add comma separated categories here", "another category"]
toc: false #if the post should include a table of contents (true, false)
displayInMenu: false #if the post should show up in the navigation menu (true, false)
displayInList: true #if the post should be listed on the home page and category pages (true, false)
resources:
- name: imgName #featuredImage
  src: imgPath #"Filename of the post's featured image, used as the card image and the image at the top of the article"
  params:
    description: imgAlt #"Description for the featured image, used as the alt text"
---

## Resumen  

{{< param "description" >}}
